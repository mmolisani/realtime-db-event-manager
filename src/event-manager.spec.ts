import { expect } from "chai";
import { RealtimeDatabase } from "realtime-db-adaptor";
import { Changeset } from "realtime-db-adaptor/lib/database";
import { Ruleset } from "realtime-db-adaptor/lib/database.spec";
import targaryen from "targaryen";
import { ChangeEventListener, ChangeEventType, DataEventListener, DataEventType, RealtimeDatabaseEventManager } from "./event-manager";

interface IndexSchema {
  [a: string]: {
    [b: string]: {
      [c: string]: {
        [d: string]: number;
      };
    };
  };
}

export type EnvironmentBuilder = <SCHEMA extends object>(data?: Changeset<SCHEMA>, ruleset?: Ruleset<SCHEMA>, eventManagerAuth?: targaryen.Auth, config?: object) => [RealtimeDatabaseEventManager<SCHEMA>, (auth: targaryen.Auth) => RealtimeDatabase<SCHEMA>];

export function runTestSuite(name: string, envBuilder: EnvironmentBuilder) {

  describe(`${name} event manager`, () => {

    describe("index schema", () => {

      describe("onDataEvent", () => {

        async function dataEventTestHandler(type: DataEventType, lookup: string[], data: IndexSchema, action: (db: RealtimeDatabase<IndexSchema>) => Promise<any>, eventHandler: DataEventListener<IndexSchema, any>): Promise<any> {
          const [eventManager, buildDatabaseWithAuth] = envBuilder<IndexSchema>(data);
          const db = buildDatabaseWithAuth(null);
          let resolve: () => void;
          let reject: (reason?: any) => void;
          const done = new Promise<void>((res, rej) => {
            resolve = res;
            reject = rej;
          });
          eventManager.onDataEvent(type, lookup as any, async (data, context, callbackDb, config) => {
            expect(callbackDb.auth).to.equal(db.auth);
            try {
              await eventHandler(data, context, callbackDb, config);
              resolve();
            } catch (err) {
              reject(err);
            }
          });
          await action(db);
          await done;
        }

        describe("create", () => {

          const createTestHandler = dataEventTestHandler.bind(void 0, "create");

          describe("/a", () => {

            const testHandler = createTestHandler.bind(void 0, ["a"], {});

            it("set /a", async () => {
              return testHandler((db) => {
                return db.set(["a"], {
                  b: {
                    c: {
                      d: 10,
                    },
                  },
                });
              }, async (data, context) => {
                expect(context.params).to.deep.equal({});
                expect(data.val()).to.deep.equal({
                  b: {
                    c: {
                      d: 10,
                    },
                  },
                });
              });
            });

          });

          describe("/a/b", () => {

            const testHandler = createTestHandler.bind(void 0, ["a", "b"], {});

            it("set /a", () => {
              return testHandler(async (db) => {
                return db.set(["a"], {
                  b: {
                    c: {
                      d: 10,
                    },
                  },
                });
              }, async (data, context) => {
                expect(context.params).to.deep.equal({});
                expect(data.val()).to.deep.equal({
                  c: {
                    d: 10,
                  },
                });
              });
            });

            it("set /a/b", () => {
              return testHandler(async (db) => {
                return db.set(["a", "b"], {
                  c: {
                    d: 10,
                  },
                });
              }, async (data, context) => {
                expect(context.params).to.deep.equal({});
                expect(data.val()).to.deep.equal({
                  c: {
                    d: 10,
                  },
                });
              });
            });

          });

          describe("/a/{b}", () => {

            const testHandler = createTestHandler.bind(void 0, ["a", "{bArg}"], {});

            it("set /a", () => {
              return testHandler(async (db) => {
                return db.set(["a"], {
                  b: {
                    c: {
                      d: 10,
                    },
                  },
                });
              }, async (data, context) => {
                expect(context.params).to.deep.equal({ bArg: "b" });
                expect(data.val()).to.deep.equal({
                  c: {
                    d: 10,
                  },
                });
              });
            });

            it("set /a/b", () => {
              return testHandler(async (db) => {
                return db.set(["a", "b"], {
                  c: {
                    d: 10,
                  },
                });
              }, async (data, context) => {
                expect(context.params).to.deep.equal({ bArg: "b" });
                expect(data.val()).to.deep.equal({
                  c: {
                    d: 10,
                  },
                });
              });
            });

            it("push /a/*", () => {
              return testHandler(async (db) => {
                return db.push(["a"], {
                  c: {
                    d: 10,
                  },
                });
              }, async (data, context) => {
                expect(context.params).to.deep.equal({ bArg: "key-0" });
                expect(data.val()).to.deep.equal({
                  c: {
                    d: 10,
                  },
                });
              });
            });

          });

          describe("/a/b/c", () => {

            const testHandler = createTestHandler.bind(void 0, ["a", "b", "c"], {});

            it("set /a", () => {
              return testHandler(async (db) => {
                return db.set(["a"], {
                  b: {
                    c: {
                      d: 10,
                    },
                  },
                });
              }, async (data, context) => {
                expect(context.params).to.deep.equal({});
                expect(data.val()).to.deep.equal({
                  d: 10,
                });
              });
            });

            it("set /a/b", () => {
              return testHandler(async (db) => {
                return db.set(["a", "b"], {
                  c: {
                    d: 10,
                  },
                });
              }, async (data, context) => {
                expect(context.params).to.deep.equal({});
                expect(data.val()).to.deep.equal({
                  d: 10,
                });
              });
            });

            it("set /a/b/c", () => {
              return testHandler(async (db) => {
                return db.set(["a", "b", "c"], {
                  d: 10,
                });
              }, async (data, context) => {
                expect(context.params).to.deep.equal({});
                expect(data.val()).to.deep.equal({
                  d: 10,
                });
              });
            });

          });

          describe("/a/b/{c}", () => {

            const testHandler = createTestHandler.bind(void 0, ["a", "b", "{cArg}"], {});

            it("set /a", () => {
              return testHandler(async (db) => {
                return db.set(["a"], {
                  b: {
                    c: {
                      d: 10,
                    },
                  },
                });
              }, async (data, context) => {
                expect(context.params).to.deep.equal({ cArg: "c" });
                expect(data.val()).to.deep.equal({
                  d: 10,
                });
              });
            });

            it("set /a/b", () => {
              return testHandler(async (db) => {
                return db.set(["a", "b"], {
                  c: {
                    d: 10,
                  },
                });
              }, async (data, context) => {
                expect(context.params).to.deep.equal({ cArg: "c" });
                expect(data.val()).to.deep.equal({
                  d: 10,
                });
              });
            });

            it("set /a/b/c", () => {
              return testHandler(async (db) => {
                return db.set(["a", "b", "c"], {
                  d: 10,
                });
              }, async (data, context) => {
                expect(context.params).to.deep.equal({ cArg: "c" });
                expect(data.val()).to.deep.equal({
                  d: 10,
                });
              });
            });

            it("push /a/b/*", () => {
              return testHandler(async (db) => {
                return db.push(["a", "b"], {
                  d: 10,
                });
              }, async (data, context) => {
                expect(context.params).to.deep.equal({ cArg: "key-0" });
                expect(data.val()).to.deep.equal({
                  d: 10,
                });
              });
            });

          });

          describe("/a/{b}/c", () => {

            const testHandler = createTestHandler.bind(void 0, ["a", "{bArg}", "c"], {});

            it("set /a", () => {
              return testHandler(async (db) => {
                return db.set(["a"], {
                  b: {
                    c: {
                      d: 10,
                    },
                  },
                });
              }, async (data, context) => {
                expect(context.params).to.deep.equal({ bArg: "b" });
                expect(data.val()).to.deep.equal({
                  d: 10,
                });
              });
            });

            it("set /a/b", () => {
              return testHandler(async (db) => {
                return db.set(["a", "b"], {
                  c: {
                    d: 10,
                  },
                });
              }, async (data, context) => {
                expect(context.params).to.deep.equal({ bArg: "b" });
                expect(data.val()).to.deep.equal({
                  d: 10,
                });
              });
            });

            it("push /a/*", () => {
              return testHandler(async (db) => {
                return db.push(["a"], {
                  c: {
                    d: 10,
                  },
                });
              }, async (data, context) => {
                expect(context.params).to.deep.equal({ bArg: "key-0" });
                expect(data.val()).to.deep.equal({
                  d: 10,
                });
              });
            });

            it("set /a/b/c", () => {
              return testHandler(async (db) => {
                return db.set(["a", "b", "c"], {
                  d: 10,
                });
              }, async (data, context) => {
                expect(context.params).to.deep.equal({ bArg: "b" });
                expect(data.val()).to.deep.equal({
                  d: 10,
                });
              });
            });

          });

          describe("/a/{b}/{c}", () => {

            const testHandler = createTestHandler.bind(void 0, ["a", "{bArg}", "{cArg}"], {});

            it("set /a", () => {
              return testHandler(async (db) => {
                return db.set(["a"], {
                  b: {
                    c: {
                      d: 10,
                    },
                  },
                });
              }, async (data, context) => {
                expect(context.params).to.deep.equal({ bArg: "b", cArg: "c" });
                expect(data.val()).to.deep.equal({
                  d: 10,
                });
              });
            });

            it("set /a/b", () => {
              return testHandler(async (db) => {
                return db.set(["a", "b"], {
                  c: {
                    d: 10,
                  },
                });
              }, async (data, context) => {
                expect(context.params).to.deep.equal({ bArg: "b", cArg: "c" });
                expect(data.val()).to.deep.equal({
                  d: 10,
                });
              });
            });

            it("push /a/*", () => {
              return testHandler(async (db) => {
                return db.push(["a"], {
                  c: {
                    d: 10,
                  },
                });
              }, async (data, context) => {
                expect(context.params).to.deep.equal({ bArg: "key-0", cArg: "c" });
                expect(data.val()).to.deep.equal({
                  d: 10,
                });
              });
            });

            it("set /a/b/c", () => {
              return testHandler(async (db) => {
                return db.set(["a", "b", "c"], {
                  d: 10,
                });
              }, async (data, context) => {
                expect(context.params).to.deep.equal({ bArg: "b", cArg: "c" });
                expect(data.val()).to.deep.equal({
                  d: 10,
                });
              });
            });

            it("push /a/b/*", () => {
              return testHandler(async (db) => {
                return db.push(["a", "b"], {
                  d: 10,
                });
              }, async (data, context) => {
                expect(context.params).to.deep.equal({ bArg: "b", cArg: "key-0" });
                expect(data.val()).to.deep.equal({
                  d: 10,
                });
              });
            });

          });

        });

        describe("delete", () => {

          const deleteTestHandler = dataEventTestHandler.bind(void 0, "delete");

          describe("/a", () => {

            const testHandler = deleteTestHandler.bind(void 0, ["a"], {
              a: {
                b: {
                  c: {
                    d: 10,
                  },
                },
              }
            });

            it("remove /a", async () => {
              return testHandler((db) => db.remove(["a"]), async (data, context) => {
                expect(context.params).to.deep.equal({});
                expect(data.val()).to.deep.equal({
                  b: {
                    c: {
                      d: 10,
                    },
                  },
                });
              });
            });

          });

          describe("/a/b", () => {

            const testHandler = deleteTestHandler.bind(void 0, ["a", "b"], {
              a: {
                b: {
                  c: {
                    d: 10,
                  },
                },
              }
            });

            it("remove /a", () => {
              return testHandler(async (db) => {
                return db.remove(["a"]);
              }, async (data, context) => {
                expect(context.params).to.deep.equal({});
                expect(data.val()).to.deep.equal({
                  c: {
                    d: 10,
                  },
                });
              });
            });

            it("remove /a/b", () => {
              return testHandler(async (db) => {
                return db.remove(["a", "b"]);
              }, async (data, context) => {
                expect(context.params).to.deep.equal({});
                expect(data.val()).to.deep.equal({
                  c: {
                    d: 10,
                  },
                });
              });
            });

          });

          describe("/a/{b}", () => {

            const testHandler = deleteTestHandler.bind(void 0, ["a", "{bArg}"], {
              a: {
                b: {
                  c: {
                    d: 10,
                  },
                },
              }
            });

            it("remove /a", () => {
              return testHandler(async (db) => {
                return db.remove(["a"]);
              }, async (data, context) => {
                expect(context.params).to.deep.equal({ bArg: "b" });
                expect(data.val()).to.deep.equal({
                  c: {
                    d: 10,
                  },
                });
              });
            });

            it("remove /a/b", () => {
              return testHandler(async (db) => {
                return db.remove(["a", "b"]);
              }, async (data, context) => {
                expect(context.params).to.deep.equal({ bArg: "b" });
                expect(data.val()).to.deep.equal({
                  c: {
                    d: 10,
                  },
                });
              });
            });

          });

          describe("/a/b/c", () => {

            const testHandler = deleteTestHandler.bind(void 0, ["a", "b", "c"], {
              a: {
                b: {
                  c: {
                    d: 10,
                  },
                },
              }
            });

            it("remove /a", () => {
              return testHandler(async (db) => {
                return db.remove(["a"]);
              }, async (data, context) => {
                expect(context.params).to.deep.equal({});
                expect(data.val()).to.deep.equal({
                  d: 10,
                });
              });
            });

            it("remove /a/b", () => {
              return testHandler(async (db) => {
                return db.remove(["a", "b"]);
              }, async (data, context) => {
                expect(context.params).to.deep.equal({});
                expect(data.val()).to.deep.equal({
                  d: 10,
                });
              });
            });

            it("remove /a/b/c", () => {
              return testHandler(async (db) => {
                return db.remove(["a", "b", "c"]);
              }, async (data, context) => {
                expect(context.params).to.deep.equal({});
                expect(data.val()).to.deep.equal({
                  d: 10,
                });
              });
            });

          });

          describe("/a/b/{c}", () => {

            const testHandler = deleteTestHandler.bind(void 0, ["a", "b", "{cArg}"], {
              a: {
                b: {
                  c: {
                    d: 10,
                  },
                },
              }
            });

            it("remove /a", () => {
              return testHandler(async (db) => {
                return db.remove(["a"]);
              }, async (data, context) => {
                expect(context.params).to.deep.equal({ cArg: "c" });
                expect(data.val()).to.deep.equal({
                  d: 10,
                });
              });
            });

            it("remove /a/b", () => {
              return testHandler(async (db) => {
                return db.remove(["a", "b"]);
              }, async (data, context) => {
                expect(context.params).to.deep.equal({ cArg: "c" });
                expect(data.val()).to.deep.equal({
                  d: 10,
                });
              });
            });

            it("remove /a/b/c", () => {
              return testHandler(async (db) => {
                return db.remove(["a", "b", "c"]);
              }, async (data, context) => {
                expect(context.params).to.deep.equal({ cArg: "c" });
                expect(data.val()).to.deep.equal({
                  d: 10,
                });
              });
            });

          });

          describe("/a/{b}/c", () => {

            const testHandler = deleteTestHandler.bind(void 0, ["a", "{bArg}", "c"], {
              a: {
                b: {
                  c: {
                    d: 10,
                  },
                },
              }
            });

            it("remove /a", () => {
              return testHandler(async (db) => {
                return db.remove(["a"]);
              }, async (data, context) => {
                expect(context.params).to.deep.equal({ bArg: "b" });
                expect(data.val()).to.deep.equal({
                  d: 10,
                });
              });
            });

            it("remove /a/b", () => {
              return testHandler(async (db) => {
                return db.remove(["a", "b"]);
              }, async (data, context) => {
                expect(context.params).to.deep.equal({ bArg: "b" });
                expect(data.val()).to.deep.equal({
                  d: 10,
                });
              });
            });

            it("remove /a/b/c", () => {
              return testHandler(async (db) => {
                return db.remove(["a", "b", "c"]);
              }, async (data, context) => {
                expect(context.params).to.deep.equal({ bArg: "b" });
                expect(data.val()).to.deep.equal({
                  d: 10,
                });
              });
            });

          });

          describe("/a/{b}/{c}", () => {

            const testHandler = deleteTestHandler.bind(void 0, ["a", "{bArg}", "{cArg}"], {
              a: {
                b: {
                  c: {
                    d: 10,
                  },
                },
              }
            });

            it("remove /a", () => {
              return testHandler(async (db) => {
                return db.remove(["a"]);
              }, async (data, context) => {
                expect(context.params).to.deep.equal({ bArg: "b", cArg: "c" });
                expect(data.val()).to.deep.equal({
                  d: 10,
                });
              });
            });

            it("remove /a/b", () => {
              return testHandler(async (db) => {
                return db.remove(["a", "b"]);
              }, async (data, context) => {
                expect(context.params).to.deep.equal({ bArg: "b", cArg: "c" });
                expect(data.val()).to.deep.equal({
                  d: 10,
                });
              });
            });

            it("remove /a/b/c", () => {
              return testHandler(async (db) => {
                return db.remove(["a", "b", "c"]);
              }, async (data, context) => {
                expect(context.params).to.deep.equal({ bArg: "b", cArg: "c" });
                expect(data.val()).to.deep.equal({
                  d: 10,
                });
              });
            });

          });

        });

      });

      describe("onChangeEvent", () => {

        async function changeEventTestHandler(type: ChangeEventType, lookup: string[], data: IndexSchema, action: (db: RealtimeDatabase<IndexSchema>) => Promise<any>, eventHandler: ChangeEventListener<IndexSchema, any>): Promise<any> {
          const [eventManager, buildDatabaseWithAuth] = envBuilder<IndexSchema>(data);
          const db = buildDatabaseWithAuth(null);
          let resolve: () => void;
          let reject: (reason?: any) => void;
          const done = new Promise<void>((res, rej) => {
            resolve = res;
            reject = rej;
          });
          eventManager.onChangeEvent(type, lookup as any, async (change, context, callbackDb, config) => {
            expect(callbackDb.auth).to.equal(db.auth);
            try {
              await eventHandler(change, context, callbackDb, config);
              resolve();
            } catch (err) {
              reject(err);
            }
          });
          await action(db);
          await done;
        }

        describe("update", () => {

          const updateTestHandler = changeEventTestHandler.bind(void 0, "update");

          describe("/a", () => {

            const testHandler = updateTestHandler.bind(void 0, ["a"], {
              a: {
                b: {
                  c: {
                    d: 111,
                    d2: 112,
                  },
                  c2: {
                    d: 121,
                    d2: 122,
                  },
                },
                b2: {
                  c: {
                    d: 211,
                    d2: 212,
                  },
                  c2: {
                    d: 221,
                    d2: 222,
                  },
                },
              },
            });

            it("set /a", async () => {
              return testHandler((db) => {
                return db.set(["a"], {
                  b: {
                    c: {
                      d: -1,
                    },
                  },
                });
              }, async (data, context) => {
                expect(context.params).to.deep.equal({});
                expect(data.before.val()).to.deep.equal({
                  b: {
                    c: {
                      d: 111,
                      d2: 112,
                    },
                    c2: {
                      d: 121,
                      d2: 122,
                    },
                  },
                  b2: {
                    c: {
                      d: 211,
                      d2: 212,
                    },
                    c2: {
                      d: 221,
                      d2: 222,
                    },
                  },
                });
                expect(data.after.val()).to.deep.equal({
                  b: {
                    c: {
                      d: -1,
                    },
                  },
                });
              });
            });

            it("set /a/b", async () => {
              return testHandler(async (db) => {
                return db.set(["a", "b"], {
                  c: {
                    d: -1,
                  },
                });
              }, async (data, context) => {
                expect(context.params).to.deep.equal({});
                expect(data.before.val()).to.deep.equal({
                  b: {
                    c: {
                      d: 111,
                      d2: 112,
                    },
                    c2: {
                      d: 121,
                      d2: 122,
                    },
                  },
                  b2: {
                    c: {
                      d: 211,
                      d2: 212,
                    },
                    c2: {
                      d: 221,
                      d2: 222,
                    },
                  },
                });
                expect(data.after.val()).to.deep.equal({
                  b: {
                    c: {
                      d: -1,
                    },
                  },
                  b2: {
                    c: {
                      d: 211,
                      d2: 212,
                    },
                    c2: {
                      d: 221,
                      d2: 222,
                    },
                  },
                });
              });
            });

            it("set /a/b/c", async () => {
              return testHandler(async (db) => {
                return db.set(["a", "b", "c"], {
                  d: -1,
                });
              }, async (data, context) => {
                expect(context.params).to.deep.equal({});
                expect(data.before.val()).to.deep.equal({
                  b: {
                    c: {
                      d: 111,
                      d2: 112,
                    },
                    c2: {
                      d: 121,
                      d2: 122,
                    },
                  },
                  b2: {
                    c: {
                      d: 211,
                      d2: 212,
                    },
                    c2: {
                      d: 221,
                      d2: 222,
                    },
                  },
                });
                expect(data.after.val()).to.deep.equal({
                  b: {
                    c: {
                      d: -1,
                    },
                    c2: {
                      d: 121,
                      d2: 122,
                    },
                  },
                  b2: {
                    c: {
                      d: 211,
                      d2: 212,
                    },
                    c2: {
                      d: 221,
                      d2: 222,
                    },
                  },
                });
              });
            });

            it("set /a/b/c/d", async () => {
              return testHandler(async (db) => {
                return db.set(["a", "b", "c", "d"], -1);
              }, async (data, context) => {
                expect(context.params).to.deep.equal({});
                expect(data.before.val()).to.deep.equal({
                  b: {
                    c: {
                      d: 111,
                      d2: 112,
                    },
                    c2: {
                      d: 121,
                      d2: 122,
                    },
                  },
                  b2: {
                    c: {
                      d: 211,
                      d2: 212,
                    },
                    c2: {
                      d: 221,
                      d2: 222,
                    },
                  },
                });
                expect(data.after.val()).to.deep.equal({
                  b: {
                    c: {
                      d: -1,
                      d2: 112,
                    },
                    c2: {
                      d: 121,
                      d2: 122,
                    },
                  },
                  b2: {
                    c: {
                      d: 211,
                      d2: 212,
                    },
                    c2: {
                      d: 221,
                      d2: 222,
                    },
                  },
                });
              });
            });

            it("push /a/*", async () => {
              return testHandler(async (db) => {
                return db.push(["a"], {
                  c: {
                    d: -1,
                  },
                });
              }, async (data, context) => {
                expect(context.params).to.deep.equal({});
                expect(data.before.val()).to.deep.equal({
                  b: {
                    c: {
                      d: 111,
                      d2: 112,
                    },
                    c2: {
                      d: 121,
                      d2: 122,
                    },
                  },
                  b2: {
                    c: {
                      d: 211,
                      d2: 212,
                    },
                    c2: {
                      d: 221,
                      d2: 222,
                    },
                  },
                });
                expect(data.after.val()).to.deep.equal({
                  b: {
                    c: {
                      d: 111,
                      d2: 112,
                    },
                    c2: {
                      d: 121,
                      d2: 122,
                    },
                  },
                  b2: {
                    c: {
                      d: 211,
                      d2: 212,
                    },
                    c2: {
                      d: 221,
                      d2: 222,
                    },
                  },
                  "key-0": {
                    c: {
                      d: -1,
                    },
                  },
                });
              });
            });

            it("push /a/b/*", async () => {
              return testHandler(async (db) => {
                return db.push(["a", "b"], {
                  d: -1,
                });
              }, async (data, context) => {
                expect(context.params).to.deep.equal({});
                expect(data.before.val()).to.deep.equal({
                  b: {
                    c: {
                      d: 111,
                      d2: 112,
                    },
                    c2: {
                      d: 121,
                      d2: 122,
                    },
                  },
                  b2: {
                    c: {
                      d: 211,
                      d2: 212,
                    },
                    c2: {
                      d: 221,
                      d2: 222,
                    },
                  },
                });
                expect(data.after.val()).to.deep.equal({
                  b: {
                    c: {
                      d: 111,
                      d2: 112,
                    },
                    c2: {
                      d: 121,
                      d2: 122,
                    },
                    "key-0": {
                      d: -1,
                    },
                  },
                  b2: {
                    c: {
                      d: 211,
                      d2: 212,
                    },
                    c2: {
                      d: 221,
                      d2: 222,
                    },
                  },
                });
              });
            });

            it("push /a/b/c/*", async () => {
              return testHandler(async (db) => {
                return db.push(["a", "b", "c"], -1);
              }, async (data, context) => {
                expect(context.params).to.deep.equal({});
                expect(data.before.val()).to.deep.equal({
                  b: {
                    c: {
                      d: 111,
                      d2: 112,
                    },
                    c2: {
                      d: 121,
                      d2: 122,
                    },
                  },
                  b2: {
                    c: {
                      d: 211,
                      d2: 212,
                    },
                    c2: {
                      d: 221,
                      d2: 222,
                    },
                  },
                });
                expect(data.after.val()).to.deep.equal({
                  b: {
                    c: {
                      d: 111,
                      d2: 112,
                      "key-0": -1,
                    },
                    c2: {
                      d: 121,
                      d2: 122,
                    },
                  },
                  b2: {
                    c: {
                      d: 211,
                      d2: 212,
                    },
                    c2: {
                      d: 221,
                      d2: 222,
                    },
                  },
                });
              });
            });

            it("remove /a/b2", async () => {
              return testHandler((db) => db.remove(["a", "b2"]), async (data, context) => {
                expect(context.params).to.deep.equal({});
                expect(data.before.val()).to.deep.equal({
                  b: {
                    c: {
                      d: 111,
                      d2: 112,
                    },
                    c2: {
                      d: 121,
                      d2: 122,
                    },
                  },
                  b2: {
                    c: {
                      d: 211,
                      d2: 212,
                    },
                    c2: {
                      d: 221,
                      d2: 222,
                    },
                  },
                });
                expect(data.after.val()).to.deep.equal({
                  b: {
                    c: {
                      d: 111,
                      d2: 112,
                    },
                    c2: {
                      d: 121,
                      d2: 122,
                    },
                  },
                });
              });
            });

            it("remove /a/b2/c2", async () => {
              return testHandler((db) => db.remove(["a", "b2", "c2"]), async (data, context) => {
                expect(context.params).to.deep.equal({});
                expect(data.before.val()).to.deep.equal({
                  b: {
                    c: {
                      d: 111,
                      d2: 112,
                    },
                    c2: {
                      d: 121,
                      d2: 122,
                    },
                  },
                  b2: {
                    c: {
                      d: 211,
                      d2: 212,
                    },
                    c2: {
                      d: 221,
                      d2: 222,
                    },
                  },
                });
                expect(data.after.val()).to.deep.equal({
                  b: {
                    c: {
                      d: 111,
                      d2: 112,
                    },
                    c2: {
                      d: 121,
                      d2: 122,
                    },
                  },
                  b2: {
                    c: {
                      d: 211,
                      d2: 212,
                    },
                  },
                });
              });
            });

            it("remove /a/b2/c2/d2", async () => {
              return testHandler((db) => db.remove(["a", "b2", "c2", "d2"]), async (data, context) => {
                expect(context.params).to.deep.equal({});
                expect(data.before.val()).to.deep.equal({
                  b: {
                    c: {
                      d: 111,
                      d2: 112,
                    },
                    c2: {
                      d: 121,
                      d2: 122,
                    },
                  },
                  b2: {
                    c: {
                      d: 211,
                      d2: 212,
                    },
                    c2: {
                      d: 221,
                      d2: 222,
                    },
                  },
                });
                expect(data.after.val()).to.deep.equal({
                  b: {
                    c: {
                      d: 111,
                      d2: 112,
                    },
                    c2: {
                      d: 121,
                      d2: 122,
                    },
                  },
                  b2: {
                    c: {
                      d: 211,
                      d2: 212,
                    },
                    c2: {
                      d: 221,
                    },
                  },
                });
              });
            });

          });

          describe("/a/b", () => {

            const testHandler = updateTestHandler.bind(void 0, ["a", "b"], {
              a: {
                b: {
                  c: {
                    d: 11,
                    d2: 12,
                  },
                  c2: {
                    d: 21,
                    d2: 22,
                  },
                },
              },
            });

            it("set /a", async () => {
              return testHandler(async (db) => {
                return db.set(["a"], {
                  b: {
                    c: {
                      d: -1,
                    },
                  },
                });
              }, async (data, context) => {
                expect(context.params).to.deep.equal({});
                expect(data.before.val()).to.deep.equal({
                  c: {
                    d: 11,
                    d2: 12,
                  },
                  c2: {
                    d: 21,
                    d2: 22,
                  },
                });
                expect(data.after.val()).to.deep.equal({
                  c: {
                    d: -1,
                  },
                });
              });
            });

            it("set /a/b", async () => {
              return testHandler(async (db) => {
                return db.set(["a", "b"], {
                  c: {
                    d: -1,
                  },
                });
              }, async (data, context) => {
                expect(context.params).to.deep.equal({});
                expect(data.before.val()).to.deep.equal({
                  c: {
                    d: 11,
                    d2: 12,
                  },
                  c2: {
                    d: 21,
                    d2: 22,
                  },
                });
                expect(data.after.val()).to.deep.equal({
                  c: {
                    d: -1,
                  },
                });
              });
            });

            it("set /a/b/c", async () => {
              return testHandler(async (db) => {
                return db.set(["a", "b", "c"], {
                  d: -1,
                });
              }, async (data, context) => {
                expect(context.params).to.deep.equal({});
                expect(data.before.val()).to.deep.equal({
                  c: {
                    d: 11,
                    d2: 12,
                  },
                  c2: {
                    d: 21,
                    d2: 22,
                  },
                });
                expect(data.after.val()).to.deep.equal({
                  c: {
                    d: -1,
                  },
                  c2: {
                    d: 21,
                    d2: 22,
                  },
                });
              });
            });

            it("set /a/b/c/d", async () => {
              return testHandler(async (db) => {
                return db.set(["a", "b", "c", "d"], -1);
              }, async (data, context) => {
                expect(context.params).to.deep.equal({});
                expect(data.before.val()).to.deep.equal({
                  c: {
                    d: 11,
                    d2: 12,
                  },
                  c2: {
                    d: 21,
                    d2: 22,
                  },
                });
                expect(data.after.val()).to.deep.equal({
                  c: {
                    d: -1,
                    d2: 12,
                  },
                  c2: {
                    d: 21,
                    d2: 22,
                  },
                });
              });
            });

            it("push /a/b/*", async () => {
              return testHandler(async (db) => {
                return db.push(["a", "b"], {
                  d: -1,
                });
              }, async (data, context) => {
                expect(context.params).to.deep.equal({});
                expect(data.before.val()).to.deep.equal({
                  c: {
                    d: 11,
                    d2: 12,
                  },
                  c2: {
                    d: 21,
                    d2: 22,
                  },
                });
                expect(data.after.val()).to.deep.equal({
                  c: {
                    d: 11,
                    d2: 12,
                  },
                  c2: {
                    d: 21,
                    d2: 22,
                  },
                  "key-0": {
                    d: -1,
                  },
                });
              });
            });

            it("push /a/b/c/*", async () => {
              return testHandler(async (db) => {
                return db.push(["a", "b", "c"], -1);
              }, async (data, context) => {
                expect(context.params).to.deep.equal({});
                expect(data.before.val()).to.deep.equal({
                  c: {
                    d: 11,
                    d2: 12,
                  },
                  c2: {
                    d: 21,
                    d2: 22,
                  },
                });
                expect(data.after.val()).to.deep.equal({
                  c: {
                    d: 11,
                    d2: 12,
                    "key-0": -1,
                  },
                  c2: {
                    d: 21,
                    d2: 22,
                  },
                });
              });
            });

            it("remove /a/b/c2", async () => {
              return testHandler(async (db) => {
                return db.remove(["a", "b", "c2"]);
              }, async (data, context) => {
                expect(context.params).to.deep.equal({});
                expect(data.before.val()).to.deep.equal({
                  c: {
                    d: 11,
                    d2: 12,
                  },
                  c2: {
                    d: 21,
                    d2: 22,
                  },
                });
                expect(data.after.val()).to.deep.equal({
                  c: {
                    d: 11,
                    d2: 12,
                  },
                });
              });
            });

            it("remove /a/b/c2/d2", async () => {
              return testHandler(async (db) => {
                return db.remove(["a", "b", "c2", "d2"]);
              }, async (data, context) => {
                expect(context.params).to.deep.equal({});
                expect(data.before.val()).to.deep.equal({
                  c: {
                    d: 11,
                    d2: 12,
                  },
                  c2: {
                    d: 21,
                    d2: 22,
                  },
                });
                expect(data.after.val()).to.deep.equal({
                  c: {
                    d: 11,
                    d2: 12,
                  },
                  c2: {
                    d: 21,
                  },
                });
              });
            });

          });

          describe("/a/{b}", () => {

            const testHandler = updateTestHandler.bind(void 0, ["a", "{bArg}"], {
              a: {
                b: {
                  c: {
                    d: 11,
                    d2: 12,
                  },
                  c2: {
                    d: 21,
                    d2: 22,
                  },
                },
              },
            });

            it("set /a", async () => {
              return testHandler(async (db) => {
                return db.set(["a"], {
                  b: {
                    c: {
                      d: -1,
                    },
                  },
                });
              }, async (data, context) => {
                expect(context.params).to.deep.equal({ bArg: "b" });
                expect(data.before.val()).to.deep.equal({
                  c: {
                    d: 11,
                    d2: 12,
                  },
                  c2: {
                    d: 21,
                    d2: 22,
                  },
                });
                expect(data.after.val()).to.deep.equal({
                  c: {
                    d: -1,
                  },
                });
              });
            });

            it("set /a/b", async () => {
              return testHandler(async (db) => {
                return db.set(["a", "b"], {
                  c: {
                    d: -1,
                  },
                });
              }, async (data, context) => {
                expect(context.params).to.deep.equal({ bArg: "b" });
                expect(data.before.val()).to.deep.equal({
                  c: {
                    d: 11,
                    d2: 12,
                  },
                  c2: {
                    d: 21,
                    d2: 22,
                  },
                });
                expect(data.after.val()).to.deep.equal({
                  c: {
                    d: -1,
                  },
                });
              });
            });

            it("set /a/b/c", async () => {
              return testHandler(async (db) => {
                return db.set(["a", "b", "c"], {
                  d: -1,
                });
              }, async (data, context) => {
                expect(context.params).to.deep.equal({ bArg: "b" });
                expect(data.before.val()).to.deep.equal({
                  c: {
                    d: 11,
                    d2: 12,
                  },
                  c2: {
                    d: 21,
                    d2: 22,
                  },
                });
                expect(data.after.val()).to.deep.equal({
                  c: {
                    d: -1,
                  },
                  c2: {
                    d: 21,
                    d2: 22,
                  },
                });
              });
            });

            it("set /a/b/c/d", async () => {
              return testHandler(async (db) => {
                return db.set(["a", "b", "c", "d"], -1);
              }, async (data, context) => {
                expect(context.params).to.deep.equal({ bArg: "b" });
                expect(data.before.val()).to.deep.equal({
                  c: {
                    d: 11,
                    d2: 12,
                  },
                  c2: {
                    d: 21,
                    d2: 22,
                  },
                });
                expect(data.after.val()).to.deep.equal({
                  c: {
                    d: -1,
                    d2: 12,
                  },
                  c2: {
                    d: 21,
                    d2: 22,
                  },
                });
              });
            });

            it("push /a/b/*", async () => {
              return testHandler(async (db) => {
                return db.push(["a", "b"], {
                  d: -1,
                });
              }, async (data, context) => {
                expect(context.params).to.deep.equal({ bArg: "b" });
                expect(data.before.val()).to.deep.equal({
                  c: {
                    d: 11,
                    d2: 12,
                  },
                  c2: {
                    d: 21,
                    d2: 22,
                  },
                });
                expect(data.after.val()).to.deep.equal({
                  c: {
                    d: 11,
                    d2: 12,
                  },
                  c2: {
                    d: 21,
                    d2: 22,
                  },
                  "key-0": {
                    d: -1,
                  },
                });
              });
            });

            it("push /a/b/c/*", async () => {
              return testHandler(async (db) => {
                return db.push(["a", "b", "c"], -1);
              }, async (data, context) => {
                expect(context.params).to.deep.equal({ bArg: "b" });
                expect(data.before.val()).to.deep.equal({
                  c: {
                    d: 11,
                    d2: 12,
                  },
                  c2: {
                    d: 21,
                    d2: 22,
                  },
                });
                expect(data.after.val()).to.deep.equal({
                  c: {
                    d: 11,
                    d2: 12,
                    "key-0": -1,
                  },
                  c2: {
                    d: 21,
                    d2: 22,
                  },
                });
              });
            });

            it("remove /a/b/c2", async () => {
              return testHandler(async (db) => {
                return db.remove(["a", "b", "c2"]);
              }, async (data, context) => {
                expect(context.params).to.deep.equal({ bArg: "b" });
                expect(data.before.val()).to.deep.equal({
                  c: {
                    d: 11,
                    d2: 12,
                  },
                  c2: {
                    d: 21,
                    d2: 22,
                  },
                });
                expect(data.after.val()).to.deep.equal({
                  c: {
                    d: 11,
                    d2: 12,
                  },
                });
              });
            });

            it("remove /a/b/c2/d2", async () => {
              return testHandler(async (db) => {
                return db.remove(["a", "b", "c2", "d2"]);
              }, async (data, context) => {
                expect(context.params).to.deep.equal({ bArg: "b" });
                expect(data.before.val()).to.deep.equal({
                  c: {
                    d: 11,
                    d2: 12,
                  },
                  c2: {
                    d: 21,
                    d2: 22,
                  },
                });
                expect(data.after.val()).to.deep.equal({
                  c: {
                    d: 11,
                    d2: 12,
                  },
                  c2: {
                    d: 21,
                  },
                });
              });
            });

          });

          describe("/a/b/c", () => {

            const testHandler = updateTestHandler.bind(void 0, ["a", "b", "c"], {
              a: {
                b: {
                  c: {
                    d: 11,
                    d2: 12,
                  },
                },
              },
            });

            it("set /a", async () => {
              return testHandler(async (db) => {
                return db.set(["a"], {
                  b: {
                    c: {
                      d: -1,
                    },
                  },
                });
              }, async (data, context) => {
                expect(context.params).to.deep.equal({});
                expect(data.before.val()).to.deep.equal({
                  d: 11,
                  d2: 12,
                });
                expect(data.after.val()).to.deep.equal({
                  d: -1,
                });
              });
            });

            it("set /a/b", async () => {
              return testHandler(async (db) => {
                return db.set(["a", "b"], {
                  c: {
                    d: -1,
                  },
                });
              }, async (data, context) => {
                expect(context.params).to.deep.equal({});
                expect(data.before.val()).to.deep.equal({
                  d: 11,
                  d2: 12,
                });
                expect(data.after.val()).to.deep.equal({
                  d: -1,
                });
              });
            });

            it("set /a/b/c", async () => {
              return testHandler(async (db) => {
                return db.set(["a", "b", "c"], {
                  d: -1,
                });
              }, async (data, context) => {
                expect(context.params).to.deep.equal({});
                expect(data.before.val()).to.deep.equal({
                  d: 11,
                  d2: 12,
                });
                expect(data.after.val()).to.deep.equal({
                  d: -1,
                });
              });
            });

            it("set /a/b/c/d", async () => {
              return testHandler(async (db) => {
                return db.set(["a", "b", "c", "d"], -1);
              }, async (data, context) => {
                expect(context.params).to.deep.equal({});
                expect(data.before.val()).to.deep.equal({
                  d: 11,
                  d2: 12,
                });
                expect(data.after.val()).to.deep.equal({
                  d: -1,
                  d2: 12,
                });
              });
            });

            it("push /a/b/c/*", async () => {
              return testHandler(async (db) => {
                return db.push(["a", "b", "c"], -1);
              }, async (data, context) => {
                expect(context.params).to.deep.equal({});
                expect(data.before.val()).to.deep.equal({
                  d: 11,
                  d2: 12,
                });
                expect(data.after.val()).to.deep.equal({
                  d: 11,
                  d2: 12,
                  "key-0": -1,
                });
              });
            });

            it("remove /a/b/c/d2", async () => {
              return testHandler(async (db) => {
                return db.remove(["a", "b", "c", "d2"]);
              }, async (data, context) => {
                expect(context.params).to.deep.equal({});
                expect(data.before.val()).to.deep.equal({
                  d: 11,
                  d2: 12,
                });
                expect(data.after.val()).to.deep.equal({
                  d: 11,
                });
              });
            });

          });

          describe("/a/b/{c}", () => {

            const testHandler = updateTestHandler.bind(void 0, ["a", "b", "{cArg}"], {
              a: {
                b: {
                  c: {
                    d: 11,
                    d2: 12,
                  },
                },
              },
            });

            it("set /a", async () => {
              return testHandler(async (db) => {
                return db.set(["a"], {
                  b: {
                    c: {
                      d: -1,
                    },
                  },
                });
              }, async (data, context) => {
                expect(context.params).to.deep.equal({ cArg: "c" });
                expect(data.before.val()).to.deep.equal({
                  d: 11,
                  d2: 12,
                });
                expect(data.after.val()).to.deep.equal({
                  d: -1,
                });
              });
            });

            it("set /a/b", async () => {
              return testHandler(async (db) => {
                return db.set(["a", "b"], {
                  c: {
                    d: -1,
                  },
                });
              }, async (data, context) => {
                expect(context.params).to.deep.equal({ cArg: "c" });
                expect(data.before.val()).to.deep.equal({
                  d: 11,
                  d2: 12,
                });
                expect(data.after.val()).to.deep.equal({
                  d: -1,
                });
              });
            });

            it("set /a/b/c", async () => {
              return testHandler(async (db) => {
                return db.set(["a", "b", "c"], {
                  d: -1,
                });
              }, async (data, context) => {
                expect(context.params).to.deep.equal({ cArg: "c" });
                expect(data.before.val()).to.deep.equal({
                  d: 11,
                  d2: 12,
                });
                expect(data.after.val()).to.deep.equal({
                  d: -1,
                });
              });
            });

            it("set /a/b/c/d", async () => {
              return testHandler(async (db) => {
                return db.set(["a", "b", "c", "d"], -1);
              }, async (data, context) => {
                expect(context.params).to.deep.equal({ cArg: "c" });
                expect(data.before.val()).to.deep.equal({
                  d: 11,
                  d2: 12,
                });
                expect(data.after.val()).to.deep.equal({
                  d: -1,
                  d2: 12,
                });
              });
            });

            it("push /a/b/c/*", async () => {
              return testHandler(async (db) => {
                return db.push(["a", "b", "c"], -1);
              }, async (data, context) => {
                expect(context.params).to.deep.equal({ cArg: "c" });
                expect(data.before.val()).to.deep.equal({
                  d: 11,
                  d2: 12,
                });
                expect(data.after.val()).to.deep.equal({
                  d: 11,
                  d2: 12,
                  "key-0": -1,
                });
              });
            });

            it("remove /a/b/c/d2", async () => {
              return testHandler(async (db) => {
                return db.remove(["a", "b", "c", "d2"]);
              }, async (data, context) => {
                expect(context.params).to.deep.equal({ cArg: "c" });
                expect(data.before.val()).to.deep.equal({
                  d: 11,
                  d2: 12,
                });
                expect(data.after.val()).to.deep.equal({
                  d: 11,
                });
              });
            });

          });

          describe("/a/{b}/c", () => {

            const testHandler = updateTestHandler.bind(void 0, ["a", "{bArg}", "c"], {
              a: {
                b: {
                  c: {
                    d: 11,
                    d2: 12,
                  },
                },
              },
            });

            it("set /a", async () => {
              return testHandler(async (db) => {
                return db.set(["a"], {
                  b: {
                    c: {
                      d: -1,
                    },
                  },
                });
              }, async (data, context) => {
                expect(context.params).to.deep.equal({ bArg: "b" });
                expect(data.before.val()).to.deep.equal({
                  d: 11,
                  d2: 12,
                });
                expect(data.after.val()).to.deep.equal({
                  d: -1,
                });
              });
            });

            it("set /a/b", async () => {
              return testHandler(async (db) => {
                return db.set(["a", "b"], {
                  c: {
                    d: -1,
                  },
                });
              }, async (data, context) => {
                expect(context.params).to.deep.equal({ bArg: "b" });
                expect(data.before.val()).to.deep.equal({
                  d: 11,
                  d2: 12,
                });
                expect(data.after.val()).to.deep.equal({
                  d: -1,
                });
              });
            });

            it("set /a/b/c", async () => {
              return testHandler(async (db) => {
                return db.set(["a", "b", "c"], {
                  d: -1,
                });
              }, async (data, context) => {
                expect(context.params).to.deep.equal({ bArg: "b" });
                expect(data.before.val()).to.deep.equal({
                  d: 11,
                  d2: 12,
                });
                expect(data.after.val()).to.deep.equal({
                  d: -1,
                });
              });
            });

            it("set /a/b/c/d", async () => {
              return testHandler(async (db) => {
                return db.set(["a", "b", "c", "d"], -1);
              }, async (data, context) => {
                expect(context.params).to.deep.equal({ bArg: "b" });
                expect(data.before.val()).to.deep.equal({
                  d: 11,
                  d2: 12,
                });
                expect(data.after.val()).to.deep.equal({
                  d: -1,
                  d2: 12,
                });
              });
            });

            it("push /a/b/c/*", async () => {
              return testHandler(async (db) => {
                return db.push(["a", "b", "c"], -1);
              }, async (data, context) => {
                expect(context.params).to.deep.equal({ bArg: "b" });
                expect(data.before.val()).to.deep.equal({
                  d: 11,
                  d2: 12,
                });
                expect(data.after.val()).to.deep.equal({
                  d: 11,
                  d2: 12,
                  "key-0": -1,
                });
              });
            });

            it("remove /a/b/c/d2", async () => {
              return testHandler(async (db) => {
                return db.remove(["a", "b", "c", "d2"]);
              }, async (data, context) => {
                expect(context.params).to.deep.equal({ bArg: "b" });
                expect(data.before.val()).to.deep.equal({
                  d: 11,
                  d2: 12,
                });
                expect(data.after.val()).to.deep.equal({
                  d: 11,
                });
              });
            });

          });

          describe("/a/{b}/{c}", () => {

            const testHandler = updateTestHandler.bind(void 0, ["a", "{bArg}", "{cArg}"], {
              a: {
                b: {
                  c: {
                    d: 11,
                    d2: 12,
                  },
                },
              },
            });

            it("set /a", async () => {
              return testHandler(async (db) => {
                return db.set(["a"], {
                  b: {
                    c: {
                      d: -1,
                    },
                  },
                });
              }, async (data, context) => {
                expect(context.params).to.deep.equal({ bArg: "b", cArg: "c" });
                expect(data.before.val()).to.deep.equal({
                  d: 11,
                  d2: 12,
                });
                expect(data.after.val()).to.deep.equal({
                  d: -1,
                });
              });
            });

            it("set /a/b", async () => {
              return testHandler(async (db) => {
                return db.set(["a", "b"], {
                  c: {
                    d: -1,
                  },
                });
              }, async (data, context) => {
                expect(context.params).to.deep.equal({ bArg: "b", cArg: "c" });
                expect(data.before.val()).to.deep.equal({
                  d: 11,
                  d2: 12,
                });
                expect(data.after.val()).to.deep.equal({
                  d: -1,
                });
              });
            });

            it("set /a/b/c", async () => {
              return testHandler(async (db) => {
                return db.set(["a", "b", "c"], {
                  d: -1,
                });
              }, async (data, context) => {
                expect(context.params).to.deep.equal({ bArg: "b", cArg: "c" });
                expect(data.before.val()).to.deep.equal({
                  d: 11,
                  d2: 12,
                });
                expect(data.after.val()).to.deep.equal({
                  d: -1,
                });
              });
            });

            it("set /a/b/c/d", async () => {
              return testHandler(async (db) => {
                return db.set(["a", "b", "c", "d"], -1);
              }, async (data, context) => {
                expect(context.params).to.deep.equal({ bArg: "b", cArg: "c" });
                expect(data.before.val()).to.deep.equal({
                  d: 11,
                  d2: 12,
                });
                expect(data.after.val()).to.deep.equal({
                  d: -1,
                  d2: 12,
                });
              });
            });

            it("push /a/b/c/*", async () => {
              return testHandler(async (db) => {
                return db.push(["a", "b", "c"], -1);
              }, async (data, context) => {
                expect(context.params).to.deep.equal({ bArg: "b", cArg: "c" });
                expect(data.before.val()).to.deep.equal({
                  d: 11,
                  d2: 12,
                });
                expect(data.after.val()).to.deep.equal({
                  d: 11,
                  d2: 12,
                  "key-0": -1,
                });
              });
            });

            it("remove /a/b/c/d2", async () => {
              return testHandler(async (db) => {
                return db.remove(["a", "b", "c", "d2"]);
              }, async (data, context) => {
                expect(context.params).to.deep.equal({ bArg: "b", cArg: "c" });
                expect(data.before.val()).to.deep.equal({
                  d: 11,
                  d2: 12,
                });
                expect(data.after.val()).to.deep.equal({
                  d: 11,
                });
              });
            });

          });

        });

      });

    });

  });

}
