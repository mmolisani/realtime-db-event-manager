import functions from "firebase-functions";
import { RealtimeDatabase } from "realtime-db-adaptor";

export interface DataSnapshot<T> extends functions.database.DataSnapshot {
  val(): T;
  child<K extends keyof T>(childPath: K): DataSnapshot<T[K]>;
}

export interface Change<T> {
  before: T;
  after: T;
}

export type DataEventType = "create" | "delete";
export type ChangeEventType = "update" | "write";

export type DataEventListener<S extends object, T> = (data: DataSnapshot<T>, context: functions.EventContext, db: RealtimeDatabase<S>, config: object) => Promise<any>;
export type ChangeEventListener<S extends object, T> = (change: Change<DataSnapshot<T>>, context: functions.EventContext, db: RealtimeDatabase<S>, config: object) => Promise<any>;

type DataEventCloudFunction<T> = functions.CloudFunction<DataSnapshot<T>>;
type ChangeEventCloudFunction<T> = functions.CloudFunction<Change<DataSnapshot<T>>>;

export interface RealtimeDatabaseEventManager<S extends object> {
  onDataEvent<A extends keyof S>(type: DataEventType, lookup: [A], listener: DataEventListener<S, S[A]>): DataEventCloudFunction<S[A]>;
  onDataEvent<A extends keyof S, B extends keyof S[A]>(type: DataEventType, lookup: [A, B], listener: DataEventListener<S, S[A][B]>): DataEventCloudFunction<S[A][B]>;
  onDataEvent<A extends keyof S, B extends keyof S[A], C extends keyof S[A][B]>(type: DataEventType, lookup: [A, B, C], listener: DataEventListener<S, S[A][B][C]>): DataEventCloudFunction<S[A][B][C]>;
  onDataEvent<A extends keyof S, B extends keyof S[A], C extends keyof S[A][B], D extends keyof S[A][B][C]>(type: DataEventType, lookup: [A, B, C, D], listener: DataEventListener<S, S[A][B][C][D]>): DataEventCloudFunction<S[A][B][C][D]>;
  onDataEvent<A extends keyof S, B extends keyof S[A], C extends keyof S[A][B], D extends keyof S[A][B][C], E extends keyof S[A][B][C][D]>(type: DataEventType, lookup: [A, B, C, D, E], listener: DataEventListener<S, S[A][B][C][D][E]>): DataEventCloudFunction<S[A][B][C][D][E]>;
  onDataEvent<A extends keyof S, B extends keyof S[A], C extends keyof S[A][B], D extends keyof S[A][B][C], E extends keyof S[A][B][C][D], F extends keyof S[A][B][C][D][E]>(type: DataEventType, lookup: [A, B, C, D, E, F], listener: DataEventListener<S, S[A][B][C][D][E][F]>): DataEventCloudFunction<S[A][B][C][D][E][F]>;
  onChangeEvent<A extends keyof S>(type: ChangeEventType, lookup: [A], listener: ChangeEventListener<S, S[A]>): ChangeEventCloudFunction<S[A]>;
  onChangeEvent<A extends keyof S, B extends keyof S[A]>(type: ChangeEventType, lookup: [A, B], listener: ChangeEventListener<S, S[A][B]>): ChangeEventCloudFunction<S[A][B]>;
  onChangeEvent<A extends keyof S, B extends keyof S[A], C extends keyof S[A][B]>(type: ChangeEventType, lookup: [A, B, C], listener: ChangeEventListener<S, S[A][B][C]>): ChangeEventCloudFunction<S[A][B][C]>;
  onChangeEvent<A extends keyof S, B extends keyof S[A], C extends keyof S[A][B], D extends keyof S[A][B][C]>(type: ChangeEventType, lookup: [A, B, C, D], listener: ChangeEventListener<S, S[A][B][C][D]>): ChangeEventCloudFunction<S[A][B][C][D]>;
  onChangeEvent<A extends keyof S, B extends keyof S[A], C extends keyof S[A][B], D extends keyof S[A][B][C], E extends keyof S[A][B][C][D]>(type: ChangeEventType, lookup: [A, B, C, D, E], listener: ChangeEventListener<S, S[A][B][C][D][E]>): ChangeEventCloudFunction<S[A][B][C][D][E]>;
  onChangeEvent<A extends keyof S, B extends keyof S[A], C extends keyof S[A][B], D extends keyof S[A][B][C], E extends keyof S[A][B][C][D], F extends keyof S[A][B][C][D][E]>(type: ChangeEventType, lookup: [A, B, C, D, E, F], listener: ChangeEventListener<S, S[A][B][C][D][E][F]>): ChangeEventCloudFunction<S[A][B][C][D][E][F]>;
}

export interface EventReference<T> {
  onCreate(handler: (data: DataSnapshot<T>, context?: functions.EventContext) => Promise<any>): DataEventCloudFunction<T>;
  onDelete(handler: (data: DataSnapshot<T>, context?: functions.EventContext) => Promise<any>): DataEventCloudFunction<T>;
  onUpdate(handler: (change: Change<DataSnapshot<T>>, context?: functions.EventContext) => Promise<any>): ChangeEventCloudFunction<T>;
  onWrite(handler: (change: Change<DataSnapshot<T>>, context?: functions.EventContext) => Promise<any>): ChangeEventCloudFunction<T>;
}

export abstract class AbstractRealtimeDatabaseEventManager<S extends object> implements RealtimeDatabaseEventManager<S> {
  constructor(protected db: RealtimeDatabase<S>) {}
  abstract getConfig(): object;
  protected abstract buildRef<A extends keyof S>(lookup: [A]): EventReference<S[A]>;
  protected abstract buildRef<A extends keyof S, B extends keyof S[A]>(lookup: [A, B]): EventReference<S[A][B]>;
  protected abstract buildRef<A extends keyof S, B extends keyof S[A], C extends keyof S[A][B]>(lookup: [A, B, C]): EventReference<S[A][B][C]>;
  protected abstract buildRef<A extends keyof S, B extends keyof S[A], C extends keyof S[A][B], D extends keyof S[A][B][C]>(lookup: [A, B, C, D]): EventReference<S[A][B][C][D]>;
  protected abstract buildRef<A extends keyof S, B extends keyof S[A], C extends keyof S[A][B], D extends keyof S[A][B][C], E extends keyof S[A][B][C][D]>(lookup: [A, B, C, D, E]): EventReference<S[A][B][C][D][E]>;
  protected abstract buildRef<A extends keyof S, B extends keyof S[A], C extends keyof S[A][B], D extends keyof S[A][B][C], E extends keyof S[A][B][C][D], F extends keyof S[A][B][C][D][E]>(lookup: [A, B, C, D, E, F]): EventReference<S[A][B][C][D][E][F]>;
  onDataEvent(type: DataEventType, lookup: string[], listener: DataEventListener<S, any>): DataEventCloudFunction<any> {
    const ref = this.buildRef(lookup as any);
    const handler = (data: DataSnapshot<any>, context?: functions.EventContext): Promise<any> => {
      if (!context) {
        return Promise.reject({ message: "No context specified for data event", data });
      }
      const config = this.getConfig();
      return listener(data, context, this.db, config);
    };
    switch (type) {
      case "create": return ref.onCreate(handler);
      case "delete": return ref.onDelete(handler);
    }
  }
  onChangeEvent(type: ChangeEventType, lookup: string[], listener: ChangeEventListener<S, any>): ChangeEventCloudFunction<any> {
    const ref = this.buildRef(lookup as any);
    const handler = (change: Change<DataSnapshot<any>>, context?: functions.EventContext): Promise<any> => {
      if (!context) {
        return Promise.reject({ message: "No context specified for change event", change });
      }
      const config = this.getConfig();
      return listener(change, context, this.db, config);
    };
    switch (type) {
      case "update": return ref.onUpdate(handler);
      case "write": return ref.onWrite(handler);
    }
  }
}
