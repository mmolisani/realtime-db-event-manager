import functions from "firebase-functions";
import { RealtimeDatabase } from "realtime-db-adaptor";
import { AbstractRealtimeDatabaseEventManager, EventReference, RealtimeDatabaseEventManager } from "./event-manager";

export const asPath = (lookup: string[]) => `/${lookup.join("/")}`;

class FirebaseRealtimeDatabaseEventManager<SCHEMA extends object> extends AbstractRealtimeDatabaseEventManager<SCHEMA> {
  getConfig(): object {
    return functions.config();
  } 
  protected buildRef(lookup: string[]): EventReference<any> {
    return functions.database.ref(asPath(lookup));
  }
}

export function buildFirebaseEventManagerFromDatabase<SCHEMA extends object>(db: RealtimeDatabase<SCHEMA>): RealtimeDatabaseEventManager<SCHEMA> {
  return new FirebaseRealtimeDatabaseEventManager<SCHEMA>(db);
}
