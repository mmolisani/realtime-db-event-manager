import { runTestSuite } from "./event-manager.spec";
import { buildMockEnvironment } from "./mock";

runTestSuite("mock", buildMockEnvironment);
