/// <reference path="../node_modules/realtime-db-adaptor/types/targaryen.d.ts"/>
import functions from "firebase-functions";
import { RealtimeDatabase } from "realtime-db-adaptor";
import { Changeset } from "realtime-db-adaptor/lib/database";
import { Ruleset } from "realtime-db-adaptor/lib/database.spec";
import { buildDummyKeygen, DEFAULT_RULES, MockDatabase, Table } from "realtime-db-adaptor/lib/mock";
import { merge, Observable, Subject } from "rxjs";
import { filter, map } from "rxjs/operators";
import targaryen from "targaryen";
import { AbstractRealtimeDatabaseEventManager, Change, DataSnapshot, EventReference, RealtimeDatabaseEventManager } from "./event-manager";
import { asPath } from "./firebase";

interface ParameterizedData<T> {
  write: boolean;
  data: T;
  params: { [id: string]: string };
}

interface ParameterizedDelta<T> {
  old: T;
  new: T;
  params: { [id: string]: string };
}

const matchAll = (str: string, regex: RegExp): string[] => {
  const matches: string[] = [];
  let match = regex.exec(str);
  while (match !== null) {
    matches.push(match[1]);
    match = regex.exec(str);
  }
  return matches;
};

interface SimpleSnapshot<T> {
  val(): T;
  child<K extends keyof T>(childPath: K): SimpleSnapshot<T[K]>;
}

const buildSnapshot = <T>(val: T): DataSnapshot<T> => {
  const snapshot = {
    val(): T {
      return val;
    },
    child<K extends keyof T>(childPath: K): SimpleSnapshot<T[K]> {
      return buildSnapshot(val[childPath]);
    },
  };
  return snapshot as DataSnapshot<T>;
};

export const enum DatabaseEventType {
  WRITE = "providers/google.firebase.database/eventTypes/ref.write",
  CREATE = "providers/google.firebase.database/eventTypes/ref.create",
  UPDATE = "providers/google.firebase.database/eventTypes/ref.update",
  DELETE = "providers/google.firebase.database/eventTypes/ref.delete",
}

const buildDataEventFromData = <T>(keygen: () => string, paramData: ParameterizedData<T>): [DataSnapshot<T>, functions.EventContext] => {
  const snapshot = buildSnapshot(paramData.data);
  const context = {
    eventId: keygen(),
    eventType: (paramData.write) ? DatabaseEventType.CREATE : DatabaseEventType.DELETE,
    timestamp: new Date().toISOString(),
    params: paramData.params,
  };
  return [snapshot, context as functions.EventContext];
};

const buildChangeEventFromDelta = <T>(keygen: () => string, paramDelta: ParameterizedDelta<T>): [Change<DataSnapshot<T>>, functions.EventContext] => {
  const before = buildSnapshot(paramDelta.old);
  const after = buildSnapshot(paramDelta.new);
  const context = {
    eventId: keygen(),
    eventType: DatabaseEventType.UPDATE,
    timestamp: new Date().toISOString(),
    params: paramDelta.params,
  };
  return [{ before, after }, context as functions.EventContext];
};

const isUpdate = <T>(delta: ParameterizedDelta<T>): boolean => {
  return JSON.stringify(delta.old) !== JSON.stringify(delta.new);
};

class MockEventReference<T> implements EventReference<T> {
  constructor(private _keygen: () => string, private _data: Observable<ParameterizedData<T>>, private _delta: Observable<ParameterizedDelta<T>>) { }
  onCreate(handler: (data: DataSnapshot<T>, context?: functions.EventContext) => Promise<any>): functions.CloudFunction<DataSnapshot<T>> {
    this._data.pipe(
      filter((data) => data.write),
      map((data) => buildDataEventFromData(this._keygen, data)),
    ).subscribe((args) => handler(...args));
    return {} as any;
  }
  onDelete(handler: (data: DataSnapshot<T>, context?: functions.EventContext) => Promise<any>): functions.CloudFunction<DataSnapshot<T>> {
    this._data.pipe(
      filter((data) => !data.write),
      map((data) => buildDataEventFromData(this._keygen, data)),
    ).subscribe((args) => handler(...args));
    return {} as any;
  }
  onUpdate(handler: (change: Change<DataSnapshot<T>>, context?: functions.EventContext) => Promise<any>): functions.CloudFunction<Change<DataSnapshot<T>>> {
    this._delta.pipe(
      filter(isUpdate),
      map((delta) => buildChangeEventFromDelta(this._keygen, delta)),
    ).subscribe((args) => handler(...args));
    return {} as any;
  }
  onWrite(handler: (change: Change<DataSnapshot<T>>, context?: functions.EventContext) => Promise<any>): functions.CloudFunction<Change<DataSnapshot<T>>> {
    const events = merge(...[
      this._delta.pipe(map((delta) => buildChangeEventFromDelta(this._keygen, delta))),
      this._data.pipe(
        map((data) => buildDataEventFromData(this._keygen, data)),
        map(([data, context]) => {
          const before = (context.eventType === DatabaseEventType.DELETE) ? data : buildSnapshot<any>(null);
          const after = (context.eventType === DatabaseEventType.CREATE) ? data : buildSnapshot<any>(null);
          return [{ before, after }, context] as const;
        })),
    ]).pipe(map((event) => {
      event[1].eventType = DatabaseEventType.WRITE;
      return event;
    }));
    events.subscribe((args) => handler(...args));
    return {} as any;
  }
}

export class MockDatabaseEventManager<SCHEMA extends object> extends AbstractRealtimeDatabaseEventManager<SCHEMA> {
  constructor(private _table: Table, private _config: object, private _keygen: () => string, db: RealtimeDatabase<SCHEMA>) {
    super(db);
  }
  getConfig(): object {
    return this._config;
  }
  protected buildRef(lookup: string[]): EventReference<any> {
    const path = asPath(lookup);
    const dataEvents = new Subject<ParameterizedData<any>>();
    const changeEvents = new Subject<ParameterizedDelta<any>>();
    const paramPattern = /{(\w+)}/g;
    const params = matchAll(path, paramPattern);
    const pathPattern = new RegExp(`^${path.replace(paramPattern, "([\\w-]+)")}$`);
    const sub = this._table.events.pipe(filter((event) => pathPattern.test(event.path))).subscribe((event) => {
      const resolvedParams: { [id: string]: string } = {};
      if (params.length > 0) {
        const paramSearch = pathPattern.exec(event.path);
        if (paramSearch !== null) {
          const paramValues = paramSearch.slice(1, paramSearch.length);
          params.forEach((param, i) => {
            resolvedParams[param] = paramValues[i];
          });
        }
      }
      if (event.delta.old === null || event.delta.old === void 0) {
        dataEvents.next({
          write: true,
          params: resolvedParams,
          data: event.delta.new,
        });
      } else if (event.delta.new === null || event.delta.new === void 0) {
        dataEvents.next({
          write: false,
          params: resolvedParams,
          data: event.delta.old,
        });
      } else {
        changeEvents.next({
          old: event.delta.old,
          new: event.delta.new,
          params: resolvedParams,
        });
      }
    });
    sub.add(() => {
      dataEvents.complete();
      changeEvents.complete();
    });
    return new MockEventReference(this._keygen, dataEvents, changeEvents);
  }
}

export function buildMockEnvironment<SCHEMA extends object>(data: Changeset<SCHEMA> = {}, rules: Ruleset<SCHEMA> = DEFAULT_RULES as Ruleset<SCHEMA>, eventManagerAuth: targaryen.Auth = null, config: object = {}): [RealtimeDatabaseEventManager<SCHEMA>, (auth: targaryen.Auth) => RealtimeDatabase<SCHEMA>] {
  const dummyKeygen = buildDummyKeygen();
  const table = new Table(data as any);
  const eventDb = new MockDatabase<SCHEMA>(table, { rules }, eventManagerAuth, dummyKeygen);
  const eventManager = new MockDatabaseEventManager<SCHEMA>(table, config, dummyKeygen, eventDb);
  const buildDatabaseWithAuth = (auth: targaryen.Auth) => new MockDatabase(table, { rules }, auth, dummyKeygen);
  return [eventManager, buildDatabaseWithAuth];
}
